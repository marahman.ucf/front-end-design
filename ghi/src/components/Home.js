import React from 'react';
// import '../App.css';
import Header from './Header';
import ExampleWorkout from './ExampleWorkout';
import './Home.css';

function Home() {
  return (
    <div className="App">
      <Header />
      <ExampleWorkout />
    </div>
  );
}

export default Home;
